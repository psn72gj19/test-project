#!/bin/env python

import solution_level1

def test_q2():
        res = solution_level1.subtraction(20,5)
        sol = 15
        assert res == sol, "test_alt-1.1 failed"

        res = solution_level1.multiplication(5,2)
        sol = 10
        assert res == sol, "test_alt-1.2 failed"


